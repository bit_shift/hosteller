package app.hosteller;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.lib.recaptcha.ReCaptcha;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.roger.catloadinglibrary.CatLoadingView;
import com.vstechlab.easyfonts.EasyFonts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Yb4singh on 23-09-2016.
 */
public class Attendance extends Fragment implements TaskDelegate,ReCaptcha.OnShowChallengeListener,ReCaptcha.OnVerifyAnswerListener{
    View v;
    RelativeLayout updateAttendance;
    RelativeLayout viewAttendance,addLgEntry,viewFine;
    ProgressDialog pd;
    String enrollment;
    EditText startDate,endDate;
    CatLoadingView catLoadingView;
    TextView textView;
    ReCaptcha reCaptcha;
    MaterialDialog dialog;
    int currentPage;


    @Override
    public void onChallengeShown(boolean shown) {

    }

    @Override
    public void onAnswerVerified(boolean success) {
        if (success) {
            Toast.makeText(getActivity(),"ho gaya", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            catLoadingView.show(getFragmentManager(),"");
            new CheckForLocalNetwork(getContext(),Attendance.this).execute();
        } else {
            Toast.makeText(getActivity(),"nahi hua", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.attendance,container,false);
        final FragmentManager fragmentManager = getFragmentManager();
        SharedPreferences settings = v.getContext().getSharedPreferences("Login", 0);
        enrollment = settings.getString("Enrollment","");
        catLoadingView = new CatLoadingView();
        textView = (TextView) v.findViewById(R.id.header1);
        textView.setTypeface(EasyFonts.droidSerifBold(getActivity()));
        final ViewPager viewPager;
        final ViewPager viewPager1;
        final ViewPager viewPager2;
        final ViewPager viewPager3;
        viewPager = (ViewPager) v.findViewById(R.id.updateattendanceviewpager);
        viewPager1 = (ViewPager) v.findViewById(R.id.viewattendanceviewpager);
        viewPager2 = (ViewPager) v.findViewById(R.id.addlgpager);
        viewPager3 = (ViewPager) v.findViewById(R.id.viewfinepager);
        updateAttendance = (RelativeLayout) v.findViewById(R.id.updateattendance);
        viewAttendance = (RelativeLayout) v.findViewById(R.id.viewatt);
        addLgEntry = (RelativeLayout) v.findViewById(R.id.addlg);
        viewFine = (RelativeLayout) v.findViewById(R.id.viewfine);
        setupViewPager(viewPager);
        setupViewPager1(viewPager1);
        setupViewPager2(viewPager2);
        setupViewPager3(viewPager3);
        final Handler handler = new Handler();
        currentPage =0;
        final Runnable update = new Runnable() {
            public void run() {

                viewPager.setCurrentItem(currentPage, true);
                viewPager1.setCurrentItem(currentPage,true);
                viewPager2.setCurrentItem(currentPage,true);
                viewPager3.setCurrentItem(currentPage,true);
                currentPage++;
                currentPage%=2;
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 0, 2000);

        updateAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // fragmentManager.beginTransaction().replace(R.id.framelayout,new Attendance()).commit();
                //pd=new ProgressDialog(getActivity());
                //pd.setMessage("Updating Attendance");
               // pd.show();
                 final String PUBLIC_KEY  = "6LfW_QwUAAAAAHHGN7WXhx0NDIBjUFXY8rwz6qVI";
                final String PRIVATE_KEY = "6LfW_QwUAAAAAEeLritDoVXo-m35HoNxIHDxVfZj";
                 dialog = new MaterialDialog.Builder(getActivity())
                        .customView(R.layout.captch,true)
                        .show();
                View view = dialog.getCustomView();
                 reCaptcha = (ReCaptcha)view.findViewById(R.id.recaptcha);
                reCaptcha.setVisibility(View.VISIBLE);
                reCaptcha.showChallengeAsync("6LfW_QwUAAAAAHHGN7WXhx0NDIBjUFXY8rwz6qVI", Attendance.this);
                final Button button = (Button) view.findViewById(R.id.recaptchbutton);
                final EditText editText = (EditText)view.findViewById(R.id.recaptchatext);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        reCaptcha.verifyAnswerAsync(PRIVATE_KEY, editText.getText().toString(), Attendance.this);
                    }
                });

            }
        });
        viewAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.framelayout,new ViewAttendance()).commit();
            }
        });
       addLgEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(getContext(),v);
            }
        });
        viewFine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return v;
    }

    @Override
    public void TaskCompletionResult(String result) {
      //  pd.dismiss();
        new AlertDialog.Builder(getContext())
                .setTitle(result)
                .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.drawable.alert)
                .show();
    }

    @Override
    public void CheckingNetworkFinish(boolean result) {
        if(result==true)
        new UpdateAttendance(v.getContext(),Attendance.this).execute(enrollment);
        else
        {
            catLoadingView.dismiss();
            new AlertDialog.Builder(getContext())
                    .setTitle("Netowrk Error")
                    .setMessage("Its seems, you are not in hostel area")
                    .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(R.drawable.alert)
                    .show();
        }
    }

    private void showPopup(final Context context , final View v) {
        int popupWidth = 200;
        int popupHeight = 150;
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .customView(R.layout.add_lg,true)
                .show();
        View view = dialog.getCustomView();
        startDate = (EditText) view.findViewById(R.id.startdate);
        endDate = (EditText) view.findViewById(R.id.endtdate);
       Button addLg =  (Button)view.findViewById(R.id.addlgbutton);
        addLg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startDate.getText().toString().length() == 0) {
                    startDate.setError("Start Date Required");
                    startDate.requestFocus();
                    return;
                }
                if (endDate.getText().toString().length() == 0) {
                    endDate.setError("End Date Required");
                    endDate.requestFocus();
                    return;
                }
                //  dialog.dismiss();
                pd = new ProgressDialog(context);
                pd.setMessage("Updating Lg");
                new updateLg(startDate.getText().toString().trim(), endDate.getText().toString().trim(), enrollment, pd).execute();
                dialog.dismiss();
                pd.show();
            }
        });


        Button cancel = (Button) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        selectedmonth++;
                        String date = selectedyear+"-"+selectedmonth+"-"+selectedday;
                        startDate.setText(date);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();  }
        });
        endDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        selectedmonth++;
                        String date = selectedyear+"-"+selectedmonth+"-"+selectedday;
                        endDate.setText(date);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();  }
        });
    }
    class UpdateAttendance extends AsyncTask<String,String,JSONObject>{
        private Context context;
        private TaskDelegate delegate;

        //  private ProgressDialog p= new ProgressDialog(context);
        public UpdateAttendance(Context context,TaskDelegate delegate){
            this.context = context;
            this.delegate= delegate;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            catLoadingView = new CatLoadingView();
            catLoadingView.show(getFragmentManager(),"");
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // p.setMessage("Loading");
            //   p.show();
            OkHttpClient client=new OkHttpClient();
            RequestBody body= new FormBody.Builder()
                    .add("enrollment",params[0])
                    .build();

            Request request = new Request.Builder()
                    .url("http://yb4singh.pythonanywhere.com/updateattendance/").post(body).build();
            Response response = null;
            try
            {
                response=client.newCall(request).execute();
                String jsonData = response.body().string();
                try {
                    Log.d("kk",jsonData);
                    JSONObject obj = new JSONObject(jsonData);
                    return obj;
                }
                catch(JSONException e)
                {
                    Log.d("kk",e.getMessage());
                    e.printStackTrace();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject aVoid) {
            super.onPostExecute(aVoid);
            String result="";
            if(aVoid==null)
                result = "Network Error";
            else {
                try {

                    result = aVoid.getString("Attendance");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(aVoid);
            delegate.TaskCompletionResult(result);
            catLoadingView.dismiss();
        }
    }
    class CheckForLocalNetwork extends AsyncTask<String, String, String>
    {
        private Context context;
        int respondseCode;
        private boolean flag = false;
        private TaskDelegate delegate;
        public CheckForLocalNetwork(Context context,TaskDelegate delegate)
        {
            this.context = context;
            this.delegate = delegate;
            flag=false;
        }
        @Override
        protected String doInBackground(String... params) {
            try {

                URL url = new URL("http://172.16.68.6:8090/");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                // urlConnection.setRequestMethod("GET");
                // urlConnection.setDoInput(true);
                //  InputStream in = urlConnection.getInputStream();

                urlConnection.setConnectTimeout(10000);
                urlConnection.setReadTimeout(10000);
                urlConnection.connect();
                respondseCode = urlConnection.getResponseCode();
                urlConnection.disconnect();
                if (respondseCode ==200 ){
                    flag=true;
                    // Toast.makeText(context, "Mila bhai", Toast.LENGTH_LONG).show();
                }
            }
            catch(IOException e)
            {
                e.printStackTrace();
                // Toast.makeText(context,"error", Toast.LENGTH_LONG).show();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            delegate.CheckingNetworkFinish(flag);
        }
    }
    class updateLg extends AsyncTask<String,String,String>
    {
        String startDate,endDate,enrollment;
        TaskDelegate delegate;
        ProgressDialog pd;
        public updateLg(String startDate,String endDate,String enrollment,ProgressDialog pd)
        {
            this.enrollment = enrollment;
            this.startDate = startDate;
            this.endDate = endDate;
            this.delegate = delegate;
            this.pd = pd;
        }

        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client=new OkHttpClient();
            RequestBody body= new FormBody.Builder()
                    .add("enrollment",enrollment)
                    .add("startDate",startDate)
                    .add("endDate",endDate)
                    .build();

            Request request = new Request.Builder()
                    .url("http://yb4singh.pythonanywhere.com/updatelg/").post(body).build();
            Response response = null;
            try
            {
                response=client.newCall(request).execute();
                String jsonData = response.body().string();
                Log.d("kk",jsonData);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            catLoadingView.dismiss();
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new UpdateAttedanceViewPager1(),"");
        adapter.addFragment(new UpdateAttendanceViewPager2(), "");
        viewPager.setAdapter(adapter);
    }
    private void setupViewPager1(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ViewAttendancePager1(),"");
        adapter.addFragment(new ViewAttendancePager2(), "");
        viewPager.setAdapter(adapter);
    }
    private void setupViewPager2(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new AddLgViewPager1(),"");
        adapter.addFragment(new AddLgViewPager2(), "");
        viewPager.setAdapter(adapter);
    }
    private void setupViewPager3(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ViewFineViewPager1(),"");
        adapter.addFragment(new ViewFineViewPager2(), "");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}


