package app.hosteller;

/**
 * Created by Yb4singh on 24-09-2016.
 */
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

class AttendanceCache {

    public View baseView;
    public TextView date;
    public TextView time;
    public TextView status;
    public LinearLayout relativeLayout;
    public AttendanceCache ( View baseView ) {
        this.baseView = baseView;
        this.date = (TextView) baseView.findViewById(R.id.date);
        this.time = (TextView) baseView.findViewById(R.id.time);
        this.status = (TextView) baseView.findViewById(R.id.status);
        this.relativeLayout = (LinearLayout) baseView.findViewById(R.id.relativeLayout);
    }

    public View getViewBase ( ) {
        return baseView;
    }
}