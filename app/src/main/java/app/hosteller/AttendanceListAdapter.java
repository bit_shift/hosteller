package app.hosteller;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Yb4singh on 24-09-2016.
 */

public class AttendanceListAdapter extends ArrayAdapter {

    int resourceId;
    List objects;
    Context context;
    LayoutInflater inflater;
    LinearLayout relativeLayoutayout;
    AttendanceListAdapter(Context context, int resourceId, List objects)
    {
        super(context,resourceId,objects);
        this.resourceId = resourceId;
        this.context = context;
        this.objects = objects;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AttendanceCache viewCache;
        AttendanceData data = (AttendanceData) getItem(position);
        if (convertView == null) {
            convertView = (LinearLayout) inflater.inflate(resourceId, null);
            viewCache = new AttendanceCache(convertView);
            convertView.setTag(viewCache);
        }
        else
        {
            convertView = (LinearLayout) convertView;
            viewCache = (AttendanceCache)  convertView.getTag();
        }
        TextView date,time,status;
        date = viewCache.date;
        time = viewCache.time;
        status = viewCache.status;
        date.setText(data.date);
        time.setText(data.time);
        status.setText(data.status);
        convertView.setPadding(2,2,2,2);
        if((data.status).equals("present"))
        {
            date.setTextColor(Color.parseColor("#005900"));
            time.setTextColor(Color.parseColor("#005900"));
            status.setTextColor(Color.parseColor("#005900"));
            ImageView i =(ImageView)convertView.findViewById(R.id.statusicon);
            i.setImageResource(R.drawable.green);
            i.setColorFilter(Color.parseColor("#005900"));
        }
        else {
            if (data.status == "absent") {
                date.setTextColor(Color.parseColor("#cc9900"));
                time.setTextColor(Color.parseColor("#cc9900"));
                status.setTextColor(Color.parseColor("#cc9900"));
                ImageView i =(ImageView)convertView.findViewById(R.id.statusicon);
                i.setImageResource(R.drawable.yellow);
                i.setColorFilter(Color.parseColor("#cc9900"));
            }
            else
            {
                if(data.status =="fined"||data.status == "fined/paid")
                {
                    date.setTextColor(Color.parseColor("#990000"));
                    time.setTextColor(Color.parseColor("#990000"));
                    status.setTextColor(Color.parseColor("#990000"));
                    ImageView i =(ImageView)convertView.findViewById(R.id.statusicon);
                    i.setImageResource(R.drawable.red);
                    i.setColorFilter(Color.parseColor("#990000"));
                }
                else
                {
                    date.setTextColor(Color.parseColor("#000099"));
                    time.setTextColor(Color.parseColor("#000099"));
                    status.setTextColor(Color.parseColor("#000099"));
                    ImageView i =(ImageView)convertView.findViewById(R.id.statusicon);
                    i.setImageResource(R.drawable.blue);
                    i.setColorFilter(Color.parseColor("#000099"));

                }
            }
        }
        return convertView;
    }
}
