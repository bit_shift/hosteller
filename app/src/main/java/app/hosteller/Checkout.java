package app.hosteller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tyrantgit.explosionfield.ExplosionField;

/**
 * Created by Yb4singh on 22-11-2016.
 */
public class Checkout extends AppCompatActivity {
    SQLiteDatabase sqLiteDatabase;
    List<CheckoutData> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout);
        final Button button = (Button) findViewById(R.id.checkoutButton);


        sqLiteDatabase = getApplicationContext().openOrCreateDatabase("CafeCart", 0, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Cart(name varchar,price double, quantity int)");
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * from Cart", null);
        list = new ArrayList<>();
        double total = 0;
        cursor.moveToFirst();
        do {
            String name = cursor.getString(0);
            double price = cursor.getDouble(1);
            int quantity = cursor.getInt(2);
            list.add(new CheckoutData(name, quantity, price));
            total += (price * quantity);
        }
        while (cursor.moveToNext());
        final ListView listView;
        listView = (ListView) findViewById(R.id.checkoutList);
        CheckOutListAdapter adapter = new CheckOutListAdapter(getApplicationContext(),list,R.layout.checkoutrow);
        listView.setAdapter(adapter);
        final TextView textView = (TextView) findViewById(R.id.totalprice);
        textView.setText("Total  =  ₹ "+total);
    }
    class CheckOutListAdapter extends ArrayAdapter{
        int resourceId;
        List objects;
        Context context;
        LayoutInflater inflater;
       CheckOutListAdapter(Context context, List objects, int resourceId)
        {
            super(context,resourceId,objects);
            this.resourceId = resourceId;
            this.context = context;
            this.objects = objects;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
           CheckoutData checkoutData = (CheckoutData)getItem(position);
            if (convertView == null) {
                convertView = (RelativeLayout) inflater.inflate(resourceId, null);
            }
            else
            {
                convertView = (RelativeLayout) convertView;
            }
            TextView foodname = (TextView) convertView.findViewById(R.id.foodname);
            TextView quantity = (TextView) convertView.findViewById(R.id.foodquantity);
            TextView total = (TextView) convertView.findViewById(R.id.total);
            foodname.setText(checkoutData.food_name);
            quantity.setText(checkoutData.price+"X"+checkoutData.quantity);
            total.setText("₹ "+checkoutData.price*checkoutData.quantity+"");
            ImageView imageView = (ImageView) convertView.findViewById(R.id.cross);
            final View c = convertView;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ExplosionField explosionField = ExplosionField.attach2Window(Checkout.this);
                    c.setBackgroundColor(Color.parseColor("#000000"));
                    explosionField.explode(c);
                    list.remove(position);
                    CheckOutListAdapter.this.notifyDataSetChanged();
                }
            });
            return convertView;
        }
    }
}
