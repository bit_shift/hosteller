package app.hosteller;

/**
 * Created by Yb4singh on 27-11-2016.
 */
public class CheckoutData {
    String food_name;
    int quantity;
    double price;

    public CheckoutData(String food_name, int quantity, double price) {
        this.food_name = food_name;
        this.quantity = quantity;
        this.price = price;
    }

}
