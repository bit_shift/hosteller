package app.hosteller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import tyrantgit.explosionfield.ExplosionField;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //startService(new Intent(Dashboard.this,UpdateAttendanceViaShake.class));


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            ExplosionField explosionField = ExplosionField.attach2Window(Dashboard.this);
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.framelayout);
            frameLayout.setBackground(getResources().getDrawable(R.drawable.wallpaper));
            explosionField.explode(frameLayout);


        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here
        int id = item.getItemId();
        FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
        Fragment attendance = new Attendance();
        Fragment food  = new Food();
        Fragment setting  = new Setting();
        Fragment noticeBoard = new NoticeBoard();
        if (id == R.id.nav_camera) {
                    fragmentManager.replace(R.id.framelayout,attendance).commitAllowingStateLoss();
        } else if (id == R.id.nav_food) {
                    fragmentManager.replace(R.id.framelayout,food).commit();

        } else if (id == R.id.notice_board) {
            fragmentManager.replace(R.id.framelayout,noticeBoard).commit();
        }
        else if( id == R.id.nav_setting)
        {
            fragmentManager.replace(R.id.framelayout,setting).commit();
        }
        else if(id == R.id.logout)
        {
            SharedPreferences settings = getApplicationContext().getSharedPreferences("Login", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("Login",0);
            editor.apply();
            startActivity(new Intent(getApplicationContext(),Login.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
