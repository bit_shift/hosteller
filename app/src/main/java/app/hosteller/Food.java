package app.hosteller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.vstechlab.easyfonts.EasyFonts;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * <p/>
 * Calculate the position of the sticky header view according to the
 * position of the first item in the ListView. When the first item is already
 * reached to top, you don't need to position the sticky header view.
 *
 * @author Nilanchala
 */
public class Food extends Fragment {

    private RelativeLayout stickyView;
    private ListView listView;
    private View heroImageView;
    private ImageView cart;
    private View stickyViewSpacer;
    private int MAX_ROWS = 20;
    private ProgressDialog progressDialog;
    private WaveSwipeRefreshLayout swipeRefreshLayout;
    TextView header;
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        v = inflater.inflate(R.layout.food, null);
        listView = (ListView) v.findViewById(R.id.listView);
        heroImageView = v.findViewById(R.id.heroImageView);
        stickyView = (RelativeLayout) v.findViewById(R.id.stickyView);
        swipeRefreshLayout = (WaveSwipeRefreshLayout) v.findViewById(R.id.swipeLayout);
        LayoutInflater inflaters = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listHeader = inflaters.inflate(R.layout.list_header, null);
        stickyViewSpacer = listHeader.findViewById(R.id.stickyViewPlaceholder);
        listView.addHeaderView(listHeader);
        swipeRefreshLayout.setRefreshing(true);
        cart = (ImageView) v.findViewById(R.id.cart);
        header = (TextView) v.findViewById(R.id.header);
        header.setTypeface(EasyFonts.droidSerifBold(getActivity()));
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getFirstVisiblePosition() == 0) {
                    View firstChild = listView.getChildAt(0);
                    int topY = 0;
                    if (firstChild != null) {
                        topY = firstChild.getTop();
                    }

                    int heroTopY = stickyViewSpacer.getTop();
                    stickyView.setY(Math.max(0, heroTopY + topY));
                    heroImageView.setY(topY * 0.5f);
                }
            }
        });
        swipeRefreshLayout.setColorSchemeColors(Color.WHITE);
        swipeRefreshLayout.setWaveColor(Color.rgb(17,42,68));
        swipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                new downloadCafeData().execute();
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),Checkout.class));
            }
        });
        new downloadCafeData().execute();
        return v;
    }
    private class downloadCafeData extends AsyncTask<String,String,Response>
    {
        @Override
        protected Response doInBackground(String... strings) {
            Request request = new Request.Builder()
                    .url("http://yb4singh.pythonanywhere.com/downloadcafedata/").get().build();
            try
            {
                OkHttpClient client = new OkHttpClient();
                Response response;
                response = client.newCall(request).execute();
                return response;
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Response response) {
            Log.d("cafeProducst",response.toString());
            List<FoodData> foodData = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(response.body().string());
                for (int i = 0; i <jsonArray.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                    Log.d("cafeProducst",jsonObject.getString("name"));
                    foodData.add(new FoodData(jsonObject.getString("name"),jsonObject.getDouble("price"),jsonObject.getInt("quantity")));
                }

                ArrayAdapter adapter = new FoodListAdapter(getContext(),R.layout.list_row,foodData);
                listView.setAdapter(adapter);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

            swipeRefreshLayout.setRefreshing(false);
            super.onPostExecute(response);
        }
    }
    class FoodListAdapter extends ArrayAdapter {
        int resourceId;
        List objects;
        Context context;
        LayoutInflater inflater;
        SQLiteDatabase sqLiteDatabase;
        RelativeLayout relativeLayoutayout;
        public FoodListAdapter(Context context, int resource, List objects) {
            super(context,resource,objects);
            this.context = context;
            this.resourceId = resource;
            this.objects = objects;
            this.inflater = LayoutInflater.from(context);
            sqLiteDatabase = context.openOrCreateDatabase("CafeCart",0,null);
            sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Cart(name varchar,price double, quantity int)");
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final FoodData data = (FoodData) getItem(position);
            if (convertView == null) {
                convertView = (LinearLayout) inflater.inflate(resourceId, null);
            }
            else
            {
                convertView = (LinearLayout) convertView;
            }
            final TextView name = (TextView) convertView.findViewById(R.id.food_name);
            TextView price = (TextView) convertView.findViewById(R.id.food_price);
            final TextView quantity = (TextView) convertView.findViewById(R.id.food_quantity);
            final TextView quantityAdded = (TextView) convertView.findViewById(R.id.quantiy_added);
            Button plus = (Button) convertView.findViewById(R.id.plus);
            Button minus = (Button) convertView.findViewById(R.id.minus);

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Integer.parseInt(quantityAdded.getText().toString()) == data.quantity)
                    {
                        MaterialDialog materialDialog = new MaterialDialog.Builder(context).title("Product Insufficient").content("Sorry No more quantity can be added").positiveText("Ok").show();
                        return ;
                    }
                    quantityAdded.setText(String.valueOf(Integer.parseInt(quantityAdded.getText().toString())+1));
                    String s = "Delete from Cart where name = '"+data.name+"'";
                    sqLiteDatabase.execSQL(s);
                    s = "Insert into Cart values('"+data.name+"',"+data.price+","+(Integer.parseInt(quantityAdded.getText().toString()))+")";
                    sqLiteDatabase.execSQL(s);
                }
            });
            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Integer.parseInt(quantityAdded.getText().toString()) == 0)
                    {
                        MaterialDialog materialDialog = new MaterialDialog.Builder(context).title("Quantity Already Minimum").content("No more quantity can be decreased").positiveText("Ok").show();
                        return;
                    }
                    quantityAdded.setText(String.valueOf(Integer.parseInt(quantityAdded.getText().toString())-1));
                    String s = "Delete from Cart where name = '"+data.name+"'";
                    sqLiteDatabase.execSQL(s);
                    if(Integer.parseInt(quantityAdded.getText().toString()) == 0)
                        return;
                    s = "Insert into Cart values('"+data.name+"',"+data.price+","+(Integer.parseInt(quantityAdded.getText().toString()))+")";
                    sqLiteDatabase.execSQL(s);
                }
            });
            name.setText(data.name);
            price.setText("\u20B9 "+data.price);
            quantity.setText(data.quantity+"Nos.");
            String query = "select * from Cart where name ='"+data.name+"';";
            Cursor cursor = sqLiteDatabase.rawQuery(query,null);
            if(cursor.getCount()>0)
            {
                cursor.moveToFirst();
                int qua = cursor.getInt(2);
                if(qua>data.quantity)
                    qua = data.quantity;
                query = "Update Cart set quantity ="+qua+",price = "+data.price+" where name ='"+data.name+"';";
                quantityAdded.setText(String.valueOf(qua));
                sqLiteDatabase.execSQL(query);
            }
            return convertView;
        }
    }

}
