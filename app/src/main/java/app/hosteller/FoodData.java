package app.hosteller;

/**
 * Created by Yb4singh on 17-11-2016.
 */
public class FoodData {
    String url,name;
    double price;
    int quantity;

    public FoodData(String name, double price, int  quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
}
