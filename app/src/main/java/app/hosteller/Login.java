package app.hosteller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;



public class Login extends AppCompatActivity implements TaskDelegate {

    Button button;
    ProgressBar p;
    WifiManager wifi;
    WifiInfo wifiInfo;
    String macadd;
    EditText enrollment,password,dob;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(getApplicationContext(),ReceiveNotification.class));
        SharedPreferences settings = getApplicationContext().getSharedPreferences("Login", 0);
        int checkLogin = settings.getInt("Login", 0);
        if(checkLogin==1)
        {
            Intent i =new Intent(getApplicationContext(),Dashboard.class);
            startActivity(i);
        }
        setContentView(R.layout.activity_login);
        button  =(Button) findViewById(R.id.button);
      //  p=(ProgressBar) findViewById(R.id.wait);

        enrollment = (EditText) findViewById(R.id.enrollment);
        password = (EditText) findViewById(R.id.password);

        wifi=(WifiManager)getSystemService(getApplicationContext().WIFI_SERVICE);
        wifiInfo = wifi.getConnectionInfo();
        macadd = wifiInfo.getMacAddress();
        macadd = encrypt(macadd);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enrollment.getText().toString().length()==0)
                {
                    enrollment.setError("Enrollment can't be empty!");
                    return ;
                }
                if(password.getText().toString().length()==0)
                {
                    password.setError("Password can't be empty!");
                    return ;
                }
                pd=new ProgressDialog(Login.this);
                pd.setMessage("Validating");
                pd.show();
                new GetToken(getApplicationContext(),enrollment.getText().toString().trim(),macadd,password.getText().toString().trim(),Login.this).execute();
            }
        });
    }
    String encrypt(String macadd)
    {
        Log.d("encry",macadd);
        char key='K';
        String encrypted = new String();
        char a[] = macadd.toCharArray();
        char b[] = macadd.toCharArray();
        for(int i=0;i<macadd.length();i++)
        {
            a[i] = (char)(a[i]^key);
        }
        for(int i=0;i<macadd.length();i++)
        {
            int w=0;
            for(int j=0;j<macadd.length();j++)
            {
                if(a[i] == a[j])
                {
                    w++;
                }
            }
            for(int  j=i;j<macadd.length();j++)
            {
                w+=((int)a[i]);
            }
            w = 33 + w%93;
            b[i] = (char)w;
        }
        encrypted = String.valueOf(b);
        Log.d("encry",encrypted);
        return encrypted;
    }
    @Override
    public void CheckingNetworkFinish(boolean result) {

    }

    @Override
    public void TaskCompletionResult(String result) {
        pd.dismiss();
        if(result.equals("Error"))
        {
            new MaterialDialog.Builder(Login.this).title("Error").content("Wrong Enrollment Or Password").positiveText("Ok").show() ;
            return ;
        }
        if(result.equals("Success"))
        {
            SharedPreferences settings = getApplicationContext().getSharedPreferences("Login", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("Login",1);
            editor.putString("Enrollment",enrollment.getText().toString().trim());
            editor.apply();
            Intent i =new Intent(getApplicationContext(),Dashboard.class);
            startActivity(i);
        }
        else
        {
            new AlertDialog.Builder(Login.this).setTitle("Error").setMessage("Device is already registered with "+result).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show() ;
        }
    }
}

class SendData extends AsyncTask<String,String,JSONObject>
{
    private Context context;
    private TaskDelegate delegate;
    String token;
    public SendData(Context context,TaskDelegate delegate,String token)
    {
        this.context=context;
        this.delegate = delegate;
        this.token = token;
    }


    @Override
    protected JSONObject doInBackground(String... params) {
  //      p = new ProgressDialog(context);
//        p.show();
        OkHttpClient client=new OkHttpClient();
        RequestBody body= new FormBody.Builder()
                .add("enrollment",params[0])
                .add("macaddress",params[1])
                .add("gcmToken",token)
                .build();
        Request request = new Request.Builder()
               .url("http://yb4singh.pythonanywhere.com/checkuser/").post(body).build();
        Response response = null;
        try
        {
            response=client.newCall(request).execute();
           String jsonData = response.body().string();
            try {
                Log.d("kk",jsonData);
                JSONObject obj = new JSONObject(jsonData);
                return obj;
            }
            catch(JSONException e)
            {
                Log.d("kk",e.getMessage());
                e.printStackTrace();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject aVoid) {
        String result="";
        try {

             result = aVoid.getString("Login");
        }
        catch (JSONException e)
        {e.printStackTrace();}
        super.onPostExecute(aVoid);
        result.trim();
        delegate.TaskCompletionResult(result);
       // p.dismiss();
    }
}

class checkForLoginJiit extends AsyncTask<String,String ,String> {
    String enrollment,password,macadd;
    Context context;
    TaskDelegate taskDelegate;
    String token;
    public checkForLoginJiit(String enrollment,String password,String macadd,Context context,TaskDelegate taskDelegate,String token){
this.enrollment= enrollment;
        this.password=  password;
        this.macadd = macadd;
        this.taskDelegate = taskDelegate;
        this.context = context;
        this.token = token;
}

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client=new OkHttpClient();
        RequestBody body= new FormBody.Builder()
                .add("txtInst","Institute")
                .add("InstCode","JIIT")
                .add("txtuType","Member Type ")
                .add("UserType","S")
                .add("txtCode","Enrollment No")
                .add("MemberCode",enrollment)
                .add("txtPin","Password/Pin")
                .add("Password",password)
                .add("BTNSubmit","Submit")
                .build();
        Request request = new Request.Builder()
                .url("https://webkiosk.jiit.ac.in/CommonFiles/UserAction.jsp").post(body).build();
        Response response = null;
        try{
            response=client.newCall(request).execute();
            return response.toString();
        }
        catch (Exception e)
        {

        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s.contains("StudentFiles/StudentPage.jsp")){
            new SendData(context,taskDelegate,token).execute(enrollment, macadd);
        }
        else
        {
            //new SendData(context,taskDelegate,token).execute(enrollment, macadd);
            taskDelegate.TaskCompletionResult("Error");
        }
    }
}


class GetToken extends AsyncTask<String, String,String>
{   Context context;
    String token,enroll,macadress,password;
    TaskDelegate delegate;
    public GetToken(Context context, String enroll, String macaddress, String password, TaskDelegate delegate)
    {
        this.context = context;
        this.enroll = enroll;
        this.macadress = macaddress;
        this.password= password;
        this.delegate = delegate;
      //  this.instanceID = instanceID;
    }
    @Override
    protected String doInBackground(String... strings) {




        try {

             token = FirebaseInstanceId.getInstance().getToken();
            OkHttpClient client=new OkHttpClient();
            RequestBody body= new FormBody.Builder()
                    .add("dev_id",enroll)
                    .add("reg_id",token)
                    .add("name",enroll)
                    .build();
            Request request = new Request.Builder()
                    .url("http://yb4singh.pythonanywhere.com/fcm/v1/devices/").post(body).build();
            Response response = null;
            try
            {
                response=client.newCall(request).execute();
                Log.d("kkk",response.toString());

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
          return token;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return token;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Toast.makeText(context,s,Toast.LENGTH_LONG).show();
         new checkForLoginJiit(enroll,password,macadress,context,delegate,s).execute();


        if(s==null)
        {
            //TODO
        }
        else
        {
        }
    }
}
