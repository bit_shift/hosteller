package app.hosteller;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Yb4singh on 29-11-2016.
 */

public class NoticeBoard extends Fragment {
    View v;
    ListView listView;
    List<String> list;
    List<String> list2;
     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    v = inflater.inflate(R.layout.notice_board,container,false);
        listView = (ListView) v.findViewById(R.id.noticeList);
         list = new ArrayList<>();
         list2 = new ArrayList<>();
         new DownloadNotice().execute();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(getActivity(),NoticeBoardFullView.class));
            }
        });
        return v;
    }
    class DownloadNotice extends AsyncTask<String,String,Response>
    {
        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client=new OkHttpClient();
            RequestBody body= new FormBody.Builder()
                    .build();
            Request request = new Request.Builder()
                    .url("http://yb4singh.pythonanywhere.com/getnotices/").post(body).build();
            Response response = null;
            try
            {
                response=client.newCall(request).execute();
               return response;

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Response response) {

            if(response == null)
                return;
            try {
                list.clear();
                list2.clear();

                JSONArray jsonArray = new JSONArray(response.body().string());
                for(int i=0;i<jsonArray.length();i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    list.add(jsonObject.getString("disc"));
                    list2.add(jsonObject.getString("img"));
                }
                ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(),
                        R.layout.noticerow, list);
                listView.setAdapter(adapter);
            }
            catch(Exception e)
            {
        Log.d("noticeB","asjd");
            }

            super.onPostExecute(response);
        }
    }
}
