package app.hosteller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

/**
 * Created by Yb4singh on 12-10-2016.
 */
public class Setting extends Fragment {
    View view;
    SwitchCompat switchCompat;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.setting,container,false);
        SharedPreferences settings = getActivity().getSharedPreferences("checked", 0);
        int checked = settings.getInt("checked", 0);
        switchCompat = (SwitchCompat) view.findViewById(R.id.update_attendance_via_shake);
        if(checked == 1)
        {
            switchCompat.setChecked(true);
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    getActivity().startService(new Intent(getActivity(),UpdateAttendanceViaShake.class));
                    SharedPreferences settings = getActivity().getSharedPreferences("checked", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("checked",1);
                    editor.apply();
                } else {
                    getActivity().stopService(new Intent(getActivity(),UpdateAttendanceViaShake.class));
                    SharedPreferences settings = getActivity().getSharedPreferences("checked", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("checked",0);
                    editor.apply();
                }
            }
        });
        return view;
    }
}
