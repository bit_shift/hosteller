package app.hosteller;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Yb4singh on 09-10-2016.
 */
public class UpdateAttendanceViaShake extends Service implements SensorEventListener,UpdateAttendanceViaShakeView {
    private static final float SHAKE_THRESHOLD_GRAVITY = 1.8F;
    private static final int SHAKE_SLOP_TIME_MS = 500;
    private static final int SHAKE_COUNT_RESET_TIME_MS = 3000;
    private long mShakeTimestamp;
    private int mShakeCount;
    boolean updating;
    Sensor acc;
    SensorManager sensorManager;

    @Override
    public void onCreate() {
        super.onCreate();
        updating = false;
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        acc = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, acc, SensorManager.SENSOR_DELAY_NORMAL);
        Toast.makeText(getApplicationContext(), "hh", Toast.LENGTH_LONG).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (updating)
            return;
        float x = event.values[0];
        float y = event.values[1];

        float z = event.values[2];

        float gX = x / SensorManager.GRAVITY_EARTH;
        float gY = y / SensorManager.GRAVITY_EARTH;
        float gZ = z / SensorManager.GRAVITY_EARTH;

        // gForce will be close to 1 when there is no movement.
        Float f = new Float(gX * gX + gY * gY + gZ * gZ);
        Double d = Math.sqrt(f.doubleValue());
        float gForce = d.floatValue();

        if (gForce > SHAKE_THRESHOLD_GRAVITY) {
            final long now = System.currentTimeMillis();
            // ignore shake events too close to each other (500ms)
            if (mShakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
                return;
            }

            // reset the shake count after 3 seconds of no shakes
            if (mShakeTimestamp + SHAKE_COUNT_RESET_TIME_MS < now) {
                mShakeCount = 0;
            }

            mShakeTimestamp = now;
            mShakeCount++;
            if (mShakeCount >= 3) {
                // updating = true;
                update(UpdateAttendanceViaShake.this);
            }
        }
    }

    public void update(UpdateAttendanceViaShakeView updateAttendanceViaShakeView) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.hosteller)
                .setContentTitle("HOSTELLER!")
                .setContentText("Updating Attendance")
                .setAutoCancel(true);
        Intent intent = new Intent(this, Dashboard.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, Intent.FILL_IN_ACTION);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
        new CheckForLocalNetwork(getApplicationContext(),updateAttendanceViaShakeView).execute();
    }

    class CheckForLocalNetwork extends AsyncTask<String, String, String> {
        private Context context;
        int respondseCode;
        private boolean flag = false;
        private UpdateAttendanceViaShakeView updateAttendanceViaShakeView;

        public CheckForLocalNetwork(Context context,UpdateAttendanceViaShakeView updateAttendanceViaShakeView) {
            this.context = context;
            this.updateAttendanceViaShakeView = updateAttendanceViaShakeView;
            flag = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                URL url = new URL("http://172.16.68.6:8090/");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                // urlConnection.setRequestMethod("GET");
                // urlConnection.setDoInput(true);
                //  InputStream in = urlConnection.getInputStream();

                urlConnection.setConnectTimeout(10000);
                urlConnection.setReadTimeout(10000);
                urlConnection.connect();
                respondseCode = urlConnection.getResponseCode();
                urlConnection.disconnect();
                if (respondseCode == 200) {
                    flag = true;
                    // Toast.makeText(context, "Mila bhai", Toast.LENGTH_LONG).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
                // Toast.makeText(context,"error", Toast.LENGTH_LONG).show();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (flag == true) {
                SharedPreferences settings = context.getSharedPreferences("Login", 0);
                String enrollment = settings.getString("Enrollment", "");
                new UpdateAttendance(context,updateAttendanceViaShakeView).execute(enrollment);
            } else {
                updateAttendanceViaShakeView.showNotification("notHostel");
            }
        }
    }

    class UpdateAttendance extends AsyncTask<String, String, JSONObject> {
        private Context context;
        private UpdateAttendanceViaShakeView updateAttendanceViaShakeView;
        //  private ProgressDialog p= new ProgressDialog(context);
        public UpdateAttendance(Context context,UpdateAttendanceViaShakeView updateAttendanceViaShakeView) {
            this.context = context;
            this.updateAttendanceViaShakeView = updateAttendanceViaShakeView;
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            // p.setMessage("Loading");
            //   p.show();
            OkHttpClient client = new OkHttpClient();
            RequestBody body = new FormBody.Builder()
                    .add("enrollment", params[0])
                    .build();

            Request request = new Request.Builder()
                    .url("http://yb4singh.pythonanywhere.com/updateattendance/").post(body).build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                String jsonData = response.body().string();
                try {
                    Log.d("kk", jsonData);
                    JSONObject obj = new JSONObject(jsonData);
                    return obj;
                } catch (JSONException e) {
                    Log.d("kk", e.getMessage());
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject aVoid) {
            super.onPostExecute(aVoid);
            String result = "";
            if (aVoid == null)
                result = "Network Error";
            else {
                try {

                    result = aVoid.getString("Attendance");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            updateAttendanceViaShakeView.showNotification(result);
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void showNotification(String a) {
        if(a.equals("notHostel"))
        {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.hosteller)
                    .setContentTitle("HOSTELLER!")
                    .setContentText("Updating Attendance Failed! Its seems you are not in hostel area!")
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText("Updating Attendance Failed! Its seems you are not in hostel area!"));
            Intent intent = new Intent(this, Dashboard.class);
            PendingIntent pi = PendingIntent.getActivity(this, 0, intent, Intent.FILL_IN_ACTION);
            mBuilder.setContentIntent(pi);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(0, mBuilder.build());
            return;
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.hosteller)
                .setContentTitle("HOSTELLER!")
                .setContentText(a)
                .setAutoCancel(true);
        Intent intent = new Intent(this, Dashboard.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, Intent.FILL_IN_ACTION);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }
}
