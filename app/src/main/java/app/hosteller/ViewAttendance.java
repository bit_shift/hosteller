package app.hosteller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Yb4singh on 24-09-2016.
 */
public class ViewAttendance extends Fragment implements TaskDelegate,TaskDelegate2{
    View v;
    Spinner stickyView;
    String strings[];
    String mm;
    List data ;
    String date;
    ProgressDialog pd;
    ListView listView;
    private View heroImageView;
    private View stickyViewSpacer;
    private WaveSwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        data = new ArrayList();
        v = inflater.inflate(R.layout.view_attendance,container,false);
        stickyView = (Spinner) v.findViewById(R.id.stickyView1);

        listView =(ListView) v.findViewById(R.id.viewattendance);
        new FetchMonth(ViewAttendance.this).execute();
        pd=new ProgressDialog(getActivity());
        pd.setMessage("Loading");

        LayoutInflater inflaters = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listHeader = inflaters.inflate(R.layout.list_header, null);
        stickyViewSpacer = listHeader.findViewById(R.id.stickyViewPlaceholder);
        stickyView.setBackgroundColor(Color.rgb(0,96,100));

        listView.addHeaderView(listHeader);
        heroImageView = v.findViewById(R.id.heroImageView);
        swipeRefreshLayout = (WaveSwipeRefreshLayout) v.findViewById(R.id.swipeLayout1);
        stickyView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                int items = stickyView.getSelectedItemPosition();
                String a = strings[items];
                SharedPreferences settings = v.getContext().getSharedPreferences("Login", 0);
                String enrollment = settings.getString("Enrollment","");
                mm = get(a);
                if(mm!="-1")
                {
                    pd=new ProgressDialog(getActivity());
                    pd.setMessage("Validating");
                    pd.show();
                    new GetAttendance(enrollment,mm,ViewAttendance.this).execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getFirstVisiblePosition() == 0) {
                    View firstChild = listView.getChildAt(0);
                    int topY = 0;
                    if (firstChild != null) {
                        topY = firstChild.getTop();
                    }

                    int heroTopY = stickyViewSpacer.getTop();
                    stickyView.setY(Math.max(0, heroTopY + topY));
                    heroImageView.setY(topY * 0.5f);
                }
            }
        });
        swipeRefreshLayout.setColorSchemeColors(Color.WHITE);
        swipeRefreshLayout.setWaveColor(Color.rgb(17,42,68));
        swipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

            }
        });


        return v;
    }

    @Override
    public void TaskCompletionResult(String result) {
        pd.dismiss();
        if(result.equals("error"))
        {
            Toast.makeText(getActivity(),"shit",Toast.LENGTH_LONG).show();
            return;
        }
        date = result;
        int month  = Integer.parseInt(result.substring(5,7));
        if(month<=6)
        {
            Spinner dropdown = (Spinner) v.findViewById(R.id.stickyView1);
            String[] items = new String[]{"January", "February", "March","April","May","June"};
            strings = items;
            SpinnerAdapter adap = new ArrayAdapter<String>(getActivity(), R.layout.spinnertextview, items);

            dropdown.setAdapter(adap);

        }
        else
        {
            Spinner dropdown = (Spinner) v.findViewById(R.id.stickyView1);
            String[] items = new String[]{"July","August","September","October","November","December"};
            strings = items;
            SpinnerAdapter adap = new ArrayAdapter<String>(getActivity(), R.layout.spinnertextview, items);

            dropdown.setAdapter(adap);
        }
    }

    @Override
    public void CheckingNetworkFinish(boolean result) {

    }

    @Override
    public void JsonParser(JSONArray jsonArrayPresent,JSONArray jsonArrayFined,JSONArray jsonArrayLg) {
        pd.dismiss();
        JSONObject temp = new JSONObject();
        int max;
        int j=1;
        data.clear();
        String year = "-1";
        String dayPart="1";
        if(jsonArrayPresent!=null) {
            for (int i = 0; i < jsonArrayPresent.length(); i++) {
                try {
                    temp = jsonArrayPresent.getJSONObject(i);
                    String day = temp.getString("date").substring(8, 10);
                    dayPart = temp.getString("date").substring(0, 8);
                    year = temp.getString("date").substring(0, 3);
                    int d = Integer.parseInt(day);
                    Log.d("kk", "" + d);
                    while (j < d) {
                        String dayPart2 = Integer.toString(j);
                        if (dayPart2.length() == 1) {
                            dayPart2 = "0" + dayPart2;
                        }
                        data.add(new AttendanceData(dayPart + dayPart2, "--:--:--", "absent"));
                        j++;
                    }
                    data.add(new AttendanceData(temp.getString("date"), temp.getString("time").substring(0, 8), "present"));
                    j++;
                } catch (Exception e) {

                }
            }
        }
        int totaldays = getDays(mm,date.substring(0, 3));
        Log.d("kk",totaldays+"");
        if(totaldays==-1)
            return ;
        if(!mm.equals(date.substring(5,7))) {
            while (j <= totaldays) {
                Log.d("kk", j + "");
                String d = Integer.toString(j);
                if (j < 10)
                    d = "0" + d;
                data.add(new AttendanceData(date.substring(0, 4) + "-" + mm + "-" + d, "--:--:--", "absent"));
                j++;
            }
        }
        else
        {
            int last = Integer.parseInt(date.substring(8,10));
            while(j<=last)
            {
                String d = ""+j;
                if (j < 10)
                    d = "0" + d;
                data.add(new AttendanceData(date.substring(0, 4) + "-" + mm + "-" +d , "--:--:--", "absent"));
                j++;
            }
        }

        if(jsonArrayFined!=null) {
           for (int i = 0; i < jsonArrayFined.length(); i++) {
                try {
                    String status = jsonArrayFined.getJSONObject(i).getString("status");
                    String date = jsonArrayFined.getJSONObject(i).getString("date");
                    int index = Integer.parseInt(date.substring(8, 10));
                    if (index >= data.size())
                        break;
                    AttendanceData attendanceData = (AttendanceData) data.get(index - 1);
                    if (status.equals("0"))
                        attendanceData.status = "fined";
                    else
                        attendanceData.status = "fined/paid";
                } catch (Exception e) {

                }

            }
        }
        if(jsonArrayLg!=null)
        {
            for(int i=0;i<jsonArrayLg.length();i++) {
                try {
                    int from_day = Integer.parseInt(jsonArrayLg.getJSONObject(i).getString("from_date").substring(8, 10));
                    int to_date = Integer.parseInt(jsonArrayLg.getJSONObject(i).getString("to_date").substring(8, 10));
                    if(from_day > to_date||to_date>j)
                        to_date =j;
                    for(int k=from_day-1;k<to_date;k++)
                    {
                        AttendanceData a = (AttendanceData) data.get(k);
                        if(!a.status.equals("present"))
                        a.status = "lg";
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
        ListView list = (ListView) v.findViewById(R.id.viewattendance);
        list.setAdapter(new AttendanceListAdapter(getContext(),R.layout.row_view,data));

    }
    int getDays(String mm,String year)
    {
        if(year.equals("-1"))
            return -1;
        int arr[] = {31,28,31,30,31,30,31,31,30,31,30,31};
        int d = Integer.parseInt(mm);
        int y = Integer.parseInt(year);
        if(y%4==0&&d==2)
            return 29;
        return arr[d];
    }
    @Override
    public void JsonParser2(JSONArray jsonArray) {
        if(jsonArray==null)
            return ;
        for(int i=0;i<jsonArray.length();i++) {
            try {
                int from_day = Integer.parseInt(jsonArray.getJSONObject(i).getString("from_date").substring(8, 10));
                int to_date = Integer.parseInt(jsonArray.getJSONObject(i).getString("to_date").substring(8, 10));
                for(int j=from_day-1;j<to_date;j++)
                {
                    AttendanceData a = (AttendanceData) data.get(j);
                    a.status = "lg";
                    data.set(j,a);
                }
            }
            catch (Exception e)
            {

            }
        }
    }

    @Override
    public void JsonParser3(JSONArray jsonArray) {

    }

    String get(String s)
    {
        switch(s){
            case "January":
                return "01";
            case "February":
                return "02";
            case "March":
                return "03";
            case "April":
                return "04";
            case "May":
                return "05";
            case "June":
                return "06";
            case "July":
                return "07";
            case "August":
                return "08";
            case "September":
                return "09";
            case "October":
                return "10";
            case "November":
                return "11";
            case "December":
                return "12";
            default:
                return "-1";
        }
    }
}
class FetchMonth extends AsyncTask<String ,String,String>{
    private TaskDelegate delegate;
    public FetchMonth(TaskDelegate delegate)
    {
        this.delegate = delegate;
    }
    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client=new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://yb4singh.pythonanywhere.com/checkmonth/").get().build();
        Response response = null;
        try
        {
            response=client.newCall(request).execute();
            return response.body().string();
        }
        catch (Exception e)
        {

        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
            String month="error";
            try {
                JSONObject obj = new JSONObject(s);
                 month = obj.getString("Month");

            }
            catch(JSONException e)
            {
                e.printStackTrace();
            }
        Log.d("kk",s);
            delegate.TaskCompletionResult(month);
    }
}
class GetLg extends AsyncTask<String,String,JSONArray>
{
    private TaskDelegate2 delegate;
    private String enrollment;
    private String month;
    JSONArray jsonArrayPresent;
    JSONArray jsonArrayFined;
    public GetLg(String enrollment,String month,TaskDelegate2 delegate,JSONArray jsonArrayPresent,JSONArray jsonArrayFined)
    {
        this.delegate = delegate;
        this.enrollment = enrollment;
        this.month = month;
        this.jsonArrayFined = jsonArrayFined;
        this.jsonArrayPresent = jsonArrayPresent;
    }
    @Override
    protected JSONArray doInBackground(String... params) {
        OkHttpClient client=new OkHttpClient();
        RequestBody body= new FormBody.Builder()
                .add("enrollment",enrollment)
                .add("month",month)
                .build();
        Request request = new Request.Builder()
                .url("http://yb4singh.pythonanywhere.com/getlg/").post(body).build();
        Response response = null;
        try
        {
            response=client.newCall(request).execute();
            String jsonData = response.body().string();
            try {
                Log.d("kk",jsonData);
                JSONArray obj = new JSONArray(jsonData);
                return obj;
            }
            catch(JSONException e)
            {
                Log.d("kk",e.getMessage());
                e.printStackTrace();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONArray jsonArrayLg) {
        super.onPostExecute(jsonArrayLg);
        delegate.JsonParser(jsonArrayPresent,jsonArrayFined,jsonArrayLg);
    }
}
class GetAttendance extends AsyncTask<String,String,JSONArray>{
    private TaskDelegate2 delegate;
    private String enrollment;
    private String month;
    public GetAttendance(String enrollment,String month,TaskDelegate2 delegate)
    {
        this.enrollment = enrollment;
        this.month= month;
        this.delegate = delegate;
    }

    @Override
    protected JSONArray doInBackground(String... params) {
        OkHttpClient client=new OkHttpClient();
        RequestBody body= new FormBody.Builder()
                .add("enrollment",enrollment)
                .add("month",month)
                .build();
        Request request = new Request.Builder()
                .url("http://yb4singh.pythonanywhere.com/getattendance/").post(body).build();
        Response response = null;
        try
        {
            response=client.newCall(request).execute();
            String jsonData = response.body().string();
            try {
                Log.d("kk",jsonData);
                JSONArray obj = new JSONArray(jsonData);
                return obj;
            }
            catch(JSONException e)
            {
                Log.d("kk",e.getMessage());
                return null;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONArray s) {
        super.onPostExecute(s);
        new getFineDetails(enrollment,month,delegate,s).execute();
    }
}

class getFineDetails extends AsyncTask<String,String,JSONArray>{
    String enrollment;
    String month;
    TaskDelegate2 taskDelegate;
    JSONArray jsonArrayPresent;
    public getFineDetails(String enrollment,String month,TaskDelegate2 taskDelegate,JSONArray jsonArray)
    {
        this.enrollment = enrollment;
        this.month = month;
        this.taskDelegate = taskDelegate;
        this.jsonArrayPresent = jsonArray;
    }
    @Override
    protected JSONArray doInBackground(String... params) {
        OkHttpClient client=new OkHttpClient();
        RequestBody body= new FormBody.Builder()
                .add("enrollment",enrollment)
                .add("month",month)
                .build();
        Request request = new Request.Builder()
                .url("http://yb4singh.pythonanywhere.com/getfinedetails/").post(body).build();
        Response response = null;
        try
        {
            response=client.newCall(request).execute();
            String jsonData = response.body().string();
            try {
                Log.d("kk",jsonData);
                JSONArray obj = new JSONArray(jsonData);
                return obj;
            }
            catch(JSONException e)
            {
                Log.d("kk",e.getMessage());
                return null;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONArray jsonArrayFined) {
        super.onPostExecute(jsonArrayFined);
        new GetLg(enrollment,month,taskDelegate,jsonArrayPresent,jsonArrayFined).execute();
    }
}
